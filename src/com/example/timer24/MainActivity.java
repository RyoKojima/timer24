package com.example.timer24;

import java.util.Calendar;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Typeface RobotoThin = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");
		TextView textView1 = (TextView)this.findViewById(R.id.textView1);

		Calendar cal = Calendar.getInstance();

		int hour   = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);

		cal.set(Calendar.HOUR_OF_DAY, 24);
		cal.add(Calendar.HOUR_OF_DAY, -hour);

		cal.set(Calendar.MINUTE, 0);
		cal.add(Calendar.MINUTE, -minute);

		cal.set(Calendar.SECOND, 0);
		cal.add(Calendar.SECOND, -second);


		int remainHour = cal.get(Calendar.HOUR_OF_DAY);
		int remainMinute = cal.get(Calendar.MINUTE);
		int remainSecond = cal.get(Calendar.SECOND);

		String remainTime = remainHour+":"+remainMinute+":"+remainSecond;

		textView1.setText(remainTime);
		textView1.setTypeface(RobotoThin);

		TimeReceiver receiver = new TimeReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(TimeService.ACTION);
		registerReceiver(receiver, filter);
	}

	@Override
	public void onStart() {
		super.onStart();
		Intent i = new Intent(this, com.example.timer24.TimeService.class);
		startService(i);
	}

	@Override
	public void onPause() {
		super.onPause();
		Intent i = new Intent(this, com.example.timer24.TimeService.class);
		stopService(i);
	}
}
