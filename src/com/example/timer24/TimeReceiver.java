package com.example.timer24;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

public class TimeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		String remainTime = intent.getStringExtra("remainTime");

		MainActivity act = (MainActivity)context;
		TextView textView1 = (TextView)act.findViewById(R.id.textView1);
		textView1.setText(remainTime);
	}
}
