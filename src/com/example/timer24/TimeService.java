package com.example.timer24;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class TimeService extends Service {
	private Timer timer;
	public static final String ACTION = "TimeService Action";

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i("LOG", "onCreate");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		timer = new Timer();

		timer.schedule(new TimerTask(){
			@Override
			public void run() {
				Intent i = new Intent(ACTION);

				Calendar cal = Calendar.getInstance();

				int hour   = cal.get(Calendar.HOUR_OF_DAY);
				int minute = cal.get(Calendar.MINUTE);
				int second = cal.get(Calendar.SECOND);

				cal.set(Calendar.HOUR_OF_DAY, 24);
				cal.add(Calendar.HOUR_OF_DAY, -hour);

				cal.set(Calendar.MINUTE, 0);
				cal.add(Calendar.MINUTE, -minute);

				cal.set(Calendar.SECOND, 0);
				cal.add(Calendar.SECOND, -second);


				int remainHour = cal.get(Calendar.HOUR_OF_DAY);
				int remainMinute = cal.get(Calendar.MINUTE);
				int remainSecond = cal.get(Calendar.SECOND);

				String remainTime = remainHour+":"+remainMinute+":"+remainSecond;

				i.putExtra("remainTime", remainTime);
                //セットしたインテントをブロードキャスト送信
				sendBroadcast(i);
			}
		}, 0, 1000);

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i("LOG", "onDestory");
		timer.cancel();
	}


}
